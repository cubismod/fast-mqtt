from os import environ
import logging
import paho.mqtt.client as mqtt
import fast
import json
from dotenv import load_dotenv

logging.basicConfig()


load_dotenv()
topic = "cubis/fast-mqtt"


def on_connect(client, _userdata, _flags, rc):
    logging.info(f"connected to MQTT with rc {str(rc)}")
    client.subscribe(topic)


def on_disconnect(_client, _userdata, rc):
    if rc != 0:
        logging.warning(f"unexpected MQTT disconnect with rc {str(rc)}")


def main():
    mc = mqtt.Client()
    mc.on_connect = on_connect
    mc.on_disconnect = on_disconnect

    port = int(environ["FM_PORT"])

    mc.connect(environ["FM_BROKER"], port)

    print("\nfast-mqtt")
    print("by cubis 2022")
    print("Runs Fast.com CLI and sends it to")
    print("{} {}".format(environ["FM_BROKER"], port))

    results = fast.run()
    logging.info(results)
    if results:
        mc.publish(topic, json.dumps(results))


if __name__ == "__main__":
    main()
