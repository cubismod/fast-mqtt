import logging
import subprocess
import json

logging.basicConfig()

# run fast speedtest then return
# json results
def run():
    results = subprocess.run(["npx", "fast-cli", "--json"], capture_output=True)
    json_res = json.loads(results.stdout.decode("utf-8"))

    # ensure we have all the fields we need
    try:
        json_res["downloadSpeed"]
        json_res["downloaded"]
        json_res["latency"]
        json_res["bufferBloat"]
        json_res["userLocation"]
        return json_res
    except:
        logging.error("Incorrect JSON output from fast-cli (npm pkg)")
