FROM node:alpine
WORKDIR /usr/src/fast-mqtt

COPY src/ .
RUN apk update && apk upgrade && apk add python3 py3-pip
RUN npm install -g fast-cli
RUN python3 -m pip install -r requirements.txt

CMD [ "python3", "main.py" ]
